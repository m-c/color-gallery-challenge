## 🚀 Color library App

1.  **¿Qué es?**

    Obtiene información sobre los colores que utiliza la cuenta de una API y los muestra en un catálogo. Al hacer click en cualquiera de los elementos, copia el código HEX del color.

2.  **¿Qué usa?**

    Decidí usar GatsbyJS ya que es un framework que es un framework que usa React en el que soy nuevo y me dió curiosdad.

    El resto lo hice sin usar frameworks o librerías externas.

3.  **¿Cómo se corre?**

    * a. Clonar el repositorio: En la terminal correr git clone https://m-c@bitbucket.org/m-c/color-gallery-challenge.git
    * b. Acceder a la carpeta: cd color-gallery-challenge
    * c. En caso de no tener instalado Gatsby: npm install -g gatsby-cli
    * d. Instalar dependencias: npm install
    * e. Iniciar el servidor: gatsby develop
    * f. Abrir el navegador y navegar a localhost:8000
