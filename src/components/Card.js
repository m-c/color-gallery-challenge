import React from 'react';

const Card = ({data, onClick}) => {
  return (
    <div className={'card'} style={{backgroundColor: data.color}} onClick={() => onClick()}>
      <div className={'card-header'}>
        {data.year}
      </div>
      <div className={'card-content'}>
        <p>{data.name}</p>
        <p><b>{data.color}</b></p>
      </div>
      <div className={'card-footer'}>
        {data.pantone_value}
      </div>
    </div>
  );
}

export default Card;
