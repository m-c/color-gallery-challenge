import React, { Component } from 'react';
import Card from './Card';

class ColorGrid extends Component {
  constructor(props) {
    super(props);
    //Initial state
    this.state = {
      error: null,
      isLoaded: false,
      colors: [],
      pages: null,
      currentPage: 1,
      colorsPerPage: null,
      total: null,
      paginationArray: null,
    }
    //Bind context
    this.handleClick = this.handleClick.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
  }

  componentDidMount() {
    const { apiURL, endpoint } = this.props;
    fetch(`${apiURL}${endpoint}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            colors: result.data,
            pages: result.total_pages,
            colorsPerPage: result.per_page,
            total: result.total,
            paginationArray: Array.from(Array(result.total_pages).keys())
          })
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  handleClick(color) {
    navigator.clipboard.writeText(color.color).then(function() {
      alert(`Se copió el HEX de:\n\n${color.name}\n\n${color.color}\n`);
    }, function(err) {
      alert(`Error al copiar el HEX de:\n\n${color.name}\n\n${color.color}\n`);
    });
  }

  handlePagination(currentPage) {
    const { apiURL, endpoint } = this.props;
    fetch(`${apiURL}${endpoint}?page=${currentPage}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            colors: result.data,
          })
        },
      )
  }

  render() {
    const { error, isLoaded, colors, paginationArray } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className={'container'}>
          <div className={'flex-grid'}>
            {colors.map(color =>
                <Card
                  key={color.id}
                  data={color}
                  onClick={() => this.handleClick(color)}
                />
              )}
          </div>
          <div className={'pagination'}>
            {paginationArray.map(n =>
              <button className={'glow-on-hover'} key={n} onClick={() => this.handlePagination(n+1)}>{`${n+1} `}</button>
            )}
          </div>
        </div>
      );
    }
  }
}

export default ColorGrid;
