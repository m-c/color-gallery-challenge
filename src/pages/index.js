import React from "react";

import Layout from "../components/layout"
import SEO from "../components/seo";
import ColorGrid from '../components/ColorGrid';

const IndexPage = () => {
  return (
    <Layout>
      <SEO title="Home" />
      <ColorGrid
        apiURL={'https://reqres.in/api'}
        endpoint={'/colors'}
      />
    </Layout>
  )
}

export default IndexPage;
